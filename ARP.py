import ipaddress
import DHCP


def arp_broadcast_DHCP_Discover():
    arp_broadcast()
    arp_Protocol_update()

def arp_Protocol_update():
    return None

def arp_broadcast():
    return DHCP.broadcast_DHCP_offer()


def DHCP_request() -> ipaddress:
    return DHCP.broadcast_DHCP_Ack()

